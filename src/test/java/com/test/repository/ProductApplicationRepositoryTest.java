package com.test.repository;

import com.test.entity.CustomerContact;
import com.test.entity.ProductApplication;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;

@DataJpaTest
@RunWith(SpringRunner.class)
public class ProductApplicationRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;
    @Autowired
    private ProductApplicationRepository productApplicationRepository;

    @Test
    public void whenFindByName_thenReturnEmployee() {
        // given
        CustomerContact customerContact = new CustomerContact();
        testEntityManager.persist(customerContact);
        testEntityManager.flush();

        ProductApplication productApplication_1 = ProductApplication.builder()
                .customerContact(customerContact)
                .productName(RandomStringUtils.randomAlphabetic(10))
                .createdOn(ZonedDateTime.now().minusDays(2).toLocalDateTime())
                .build();

        ProductApplication productApplication_2 = ProductApplication.builder()
                .customerContact(customerContact)
                .productName(RandomStringUtils.randomAlphabetic(10))
                .createdOn(ZonedDateTime.now().minusDays(1).toLocalDateTime())
                .build();

        ProductApplication productApplication_3 = ProductApplication.builder()
                .customerContact(customerContact)
                .productName(RandomStringUtils.randomAlphabetic(10))
                .createdOn(ZonedDateTime.now().toLocalDateTime())
                .build();

        testEntityManager.persist(productApplication_1);
        testEntityManager.persist(productApplication_2);
        testEntityManager.persist(productApplication_3);
        testEntityManager.flush();

        // when
        ProductApplication result = productApplicationRepository.findFirstByCustomerContactOrderByCreatedOnDesc(customerContact);

        // then
        assertEquals(result.getApplicationId(), productApplication_3.getApplicationId());
        assertEquals(result.getCreatedOn(), productApplication_3.getCreatedOn());
        assertEquals(result.getProductName(), productApplication_3.getProductName());
        assertEquals(result.getCustomerContact().getContactId(), productApplication_3.getCustomerContact().getContactId());
    }
}
