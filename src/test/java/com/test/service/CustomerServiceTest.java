package com.test.service;

import com.test.dto.LastApplication;
import com.test.entity.CustomerContact;
import com.test.entity.ProductApplication;
import com.test.exception.CustomerContactNotExistException;
import com.test.repository.CustomerContactRepository;
import com.test.repository.ProductApplicationRepository;
import com.test.service.impl.CustomerServiceImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    @Mock
    private CustomerContactRepository customerContactRepository;
    @Mock
    private ProductApplicationRepository productApplicationRepository;

    private CustomerService customerService;
    private ProductApplication productApplication;

    @Before
    public void setUp() {
        customerService = new CustomerServiceImpl(customerContactRepository, productApplicationRepository);

        CustomerContact customerContact = new CustomerContact();
        when(customerContactRepository.findById(1L)).thenReturn(Optional.of(customerContact));

        productApplication = ProductApplication.builder()
                .customerContact(customerContact)
                .productName(RandomStringUtils.randomAlphabetic(10))
                .createdOn(ZonedDateTime.now().toLocalDateTime())
                .build();

        when(productApplicationRepository.findFirstByCustomerContactOrderByCreatedOnDesc(customerContact)).thenReturn(productApplication);
    }

    @Test
    public void getLastCustomerProductApplication() throws CustomerContactNotExistException {
        LastApplication result = customerService.getLastCustomerProductApplication(1L);

        assertEquals(result.getContactId(), Long.valueOf(1));
        assertEquals(result.getApplicationId(), productApplication.getApplicationId());
        assertEquals(result.getApplicationCreatedOn(), productApplication.getCreatedOn());
        assertEquals(result.getProductName(),productApplication.getProductName());
    }

    @Test(expected = CustomerContactNotExistException.class)
    public void getLastCustomerProductApplication_NotFound() throws CustomerContactNotExistException {
        when(customerContactRepository.findById(1L)).thenReturn(Optional.empty());
        customerService.getLastCustomerProductApplication(1L);
    }
}
