package com.test.controller

import com.test.Application
import com.test.entity.CustomerContact
import com.test.entity.ProductApplication
import com.test.repository.CustomerContactRepository
import com.test.repository.ProductApplicationRepository
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import static org.hamcrest.CoreMatchers.equalTo
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@Transactional
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@WebAppConfiguration
class CustomerControllerIntegrationTest extends Specification {

    @Autowired
    private MockMvc mvc
    @Autowired
    private CustomerContactRepository customerContactRepository
    @Autowired
    private ProductApplicationRepository productApplicationRepository

    private CustomerContact customerContact_1
    private CustomerContact customerContact_2
    private ProductApplication targetApplication_1
    private ProductApplication targetApplication_2

    def setup() {
        customerContact_1 = new CustomerContact()
        customerContact_1 = customerContactRepository.save(customerContact_1)

        productApplicationRepository.save(new ProductApplication(customerContact: customerContact_1, productName: rand(), createdOn: nowMinusDays(3)))
        productApplicationRepository.save(new ProductApplication(customerContact: customerContact_1, productName: rand(), createdOn: nowMinusDays(2)))
        productApplicationRepository.save(new ProductApplication(customerContact: customerContact_1, productName: rand(), createdOn: nowMinusDays(1)))
        targetApplication_1 = productApplicationRepository.save(new ProductApplication(customerContact: customerContact_1, productName: rand(), createdOn: nowMinusDays(0)))

        customerContact_2 = new CustomerContact()
        customerContact_2 = customerContactRepository.save(customerContact_2)
        targetApplication_2 = productApplicationRepository.save(new ProductApplication(customerContact: customerContact_2, productName: rand(), createdOn: nowMinusDays(0)))
        productApplicationRepository.save(new ProductApplication(customerContact: customerContact_2, productName: rand(), createdOn: nowMinusDays(1)))
        productApplicationRepository.save(new ProductApplication(customerContact: customerContact_2, productName: rand(), createdOn: nowMinusDays(2)))
        productApplicationRepository.save(new ProductApplication(customerContact: customerContact_2, productName: rand(), createdOn: nowMinusDays(3)))
    }

    def "find last customer product application : for first customer"() throws Exception {
        expect:
        mvc.perform(get("/customer/applications/last")
                .param("customerId", customerContact_1.getContactId().toString())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("\$.CONTACT_ID", equalTo(customerContact_1.getContactId().intValue())))
                .andExpect(jsonPath("\$.APPLICATION_ID", equalTo(targetApplication_1.getApplicationId().intValue())))
                .andExpect(jsonPath("\$.DT_CREATED", equalTo(targetApplication_1.getCreatedOn().format(DateTimeFormatter.ISO_DATE_TIME))))
                .andExpect(jsonPath("\$.PRODUCT_NAME", equalTo(targetApplication_1.getProductName())))
    }

    def "find last customer product application : for second customer"() throws Exception {
        expect:
        mvc.perform(get("/customer/applications/last")
                .param("customerId", customerContact_2.getContactId().toString())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("\$.CONTACT_ID", equalTo(customerContact_2.getContactId().intValue())))
                .andExpect(jsonPath("\$.APPLICATION_ID", equalTo(targetApplication_2.getApplicationId().intValue())))
                .andExpect(jsonPath("\$.DT_CREATED", equalTo(targetApplication_2.getCreatedOn().format(DateTimeFormatter.ISO_DATE_TIME))))
                .andExpect(jsonPath("\$.PRODUCT_NAME", equalTo(targetApplication_2.getProductName())))
    }

    def "find last customer product application : customer not found"() throws Exception {
        expect:
        mvc.perform(get("/customer/applications/last")
                .param("customerId", "3")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
    }

    def rand() {
        return RandomStringUtils.randomAlphabetic(10)
    }

    def nowMinusDays(int days) {
        return ZonedDateTime.now().minusDays(days).toLocalDateTime()
    }
}
