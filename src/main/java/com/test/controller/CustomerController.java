package com.test.controller;

import com.test.dto.LastApplication;
import com.test.exception.CustomerContactNotExistException;
import com.test.service.CustomerService;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {

    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer/applications/last")
    HttpEntity<LastApplication> findLastCustomerProductApplication(@RequestParam(name = "customerId") Long customerId)
            throws CustomerContactNotExistException {
        return ResponseEntity.ok().body(customerService.getLastCustomerProductApplication(customerId));
    }
}
