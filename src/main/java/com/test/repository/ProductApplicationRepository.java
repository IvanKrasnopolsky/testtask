package com.test.repository;

import com.test.entity.CustomerContact;
import com.test.entity.ProductApplication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductApplicationRepository extends JpaRepository<ProductApplication, Long> {

    ProductApplication findFirstByCustomerContactOrderByCreatedOnDesc(CustomerContact customerContact);
}
