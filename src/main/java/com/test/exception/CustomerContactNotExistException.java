package com.test.exception;

public class CustomerContactNotExistException  extends Exception {

    public CustomerContactNotExistException(String message) {
        super(message);
    }
}
