package com.test.service;

import com.test.dto.LastApplication;
import com.test.exception.CustomerContactNotExistException;

public interface CustomerService {

    LastApplication getLastCustomerProductApplication(Long customerId) throws CustomerContactNotExistException;
}
