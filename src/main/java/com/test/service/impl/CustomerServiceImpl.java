package com.test.service.impl;

import com.test.dto.LastApplication;
import com.test.entity.CustomerContact;
import com.test.entity.ProductApplication;
import com.test.exception.CustomerContactNotExistException;
import com.test.repository.CustomerContactRepository;
import com.test.repository.ProductApplicationRepository;
import com.test.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerContactRepository customerContactRepository;
    private ProductApplicationRepository productApplicationRepository;

    public CustomerServiceImpl(CustomerContactRepository customerContactRepository,
                               ProductApplicationRepository productApplicationRepository) {
        this.customerContactRepository = customerContactRepository;
        this.productApplicationRepository = productApplicationRepository;
    }

    @Override
    public LastApplication getLastCustomerProductApplication(Long customerId) throws CustomerContactNotExistException {
        Optional<CustomerContact> customerContact = customerContactRepository.findById(customerId);
        if (customerContact.isEmpty()) {
            throw new CustomerContactNotExistException("Customer contact with id: " + customerId + " doesn't exist.");
        }
        ProductApplication application = productApplicationRepository.findFirstByCustomerContactOrderByCreatedOnDesc(customerContact.get());

        LastApplication lastApplication = new LastApplication();
        lastApplication.setContactId(customerId);
        lastApplication.setApplicationId(application.getApplicationId());
        lastApplication.setApplicationCreatedOn(application.getCreatedOn());
        lastApplication.setProductName(application.getProductName());

        return lastApplication;
    }
}
