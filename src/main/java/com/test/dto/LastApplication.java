package com.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@JacksonXmlRootElement(localName = "LAST_APPLICATION")
public class LastApplication {

    @JsonProperty("CONTACT_ID")
    private Long contactId;

    @JsonProperty("APPLICATION_ID")
    private Long applicationId;

    @JsonProperty("DT_CREATED")
    private LocalDateTime applicationCreatedOn;

    @JsonProperty("PRODUCT_NAME")
    private String productName;
}
