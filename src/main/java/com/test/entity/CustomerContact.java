package com.test.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "CUSTOMER_CONTACTS")
public class CustomerContact {

    @Id
    @GeneratedValue
    @Column(name = "CONTACT_ID")
    private Long contactId;

    @OneToMany(mappedBy = "customerContact")
    private List<ProductApplication> requests;
}
