package com.test.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PRODUCT_APPLICATIONS")
public class ProductApplication {

    @Id
    @GeneratedValue
    @Column(name = "APPLICATION_ID")
    private Long applicationId;

    @ManyToOne
    @JoinColumn(name = "CONTACT_ID")
    private CustomerContact customerContact;

    @Column(name = "DT_CREATED")
    private LocalDateTime createdOn;

    @Column(name = "PRODUCT_NAME")
    private String productName;
}
