package com.test;

import com.test.entity.CustomerContact;
import com.test.entity.ProductApplication;
import com.test.repository.CustomerContactRepository;
import com.test.repository.ProductApplicationRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.ZonedDateTime;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner initDatabase(ProductApplicationRepository applicationRepository,
                                   CustomerContactRepository customerContactRepository) {
        CustomerContact customerContact_1 = new CustomerContact();
        CustomerContact customerContact_2 = new CustomerContact();

        return args -> {
            customerContactRepository.save(customerContact_1);
            customerContactRepository.save(customerContact_2);
            applicationRepository.save(ProductApplication.builder()
                    .customerContact(customerContact_1)
                    .productName("Product_1")
                    .createdOn(ZonedDateTime.now().minusDays(2).toLocalDateTime())
                    .build());
            applicationRepository.save(ProductApplication.builder()
                    .customerContact(customerContact_1)
                    .productName("Product_2")
                    .createdOn(ZonedDateTime.now().minusDays(1).toLocalDateTime())
                    .build());
            applicationRepository.save(ProductApplication.builder()
                    .customerContact(customerContact_1)
                    .productName("Product_3")
                    .createdOn(ZonedDateTime.now().toLocalDateTime())
                    .build());
            applicationRepository.save(ProductApplication.builder()
                    .customerContact(customerContact_2)
                    .productName("Product_4")
                    .createdOn(ZonedDateTime.now().minusDays(1).toLocalDateTime())
                    .build());
            applicationRepository.save(ProductApplication.builder()
                    .customerContact(customerContact_2)
                    .productName("Product_5")
                    .createdOn(ZonedDateTime.now().toLocalDateTime())
                    .build());
        };
    }
}
